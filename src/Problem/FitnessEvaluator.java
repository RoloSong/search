package Problem;

/**
 * Created by rolo on 2016/12/24.
 */
public class FitnessEvaluator extends evaluator {
    public FitnessEvaluator(Problem problem){
        super(problem);
    }
    @Override
    public int evaluation(Node current) {
        int fitness = 0;
        for(int indexa = 0; indexa < current.getData().length; indexa++){
            for(int indexb = indexa; indexb < current.getData().length; indexb++){
                if(current.getData()[indexa] != current.getData()[indexb] &&
                        (!problem.checkBias(indexa, current.getData()[indexa], indexb, current.getData()[indexb])))
                    fitness++;
            }
        }
        return fitness;
    }
}
