package Problem;

/**
 * Default Consumption Evaluator is used for hill-climbing method
 * It evaluates current node and return the number of Queens that capable to attack each other
 * Created by rolo on 2016/12/20.
 */
public class DefaultConsumptionEvaluator extends evaluator {
    public DefaultConsumptionEvaluator(Problem problem){
        super(problem);
    }

    /**
     *
     * @param current the node that represent current state
     * @return the number of queens that can attack each other
     */
    @Override
    public int evaluation(Node current) {
        int result = 0;
        for(int indexa = 0; indexa < current.getData().length; indexa++){
            if(current.getData()[indexa] != Node.UNDECIDED){
                for(int indexb = indexa+1; indexb < current.getData().length; indexb++){
                    if(current.getData()[indexb] != Node.UNDECIDED){
                        if(current.getData()[indexa] == current.getData()[indexb] ||
                                Problem.checkBias(indexa,current.getData()[indexa],
                                        indexb,current.getData()[indexb]))
                            result += 1;

                    }
                }
            }
        }
        return result;
    }
}
