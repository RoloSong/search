package Problem;

/**
 * Created by rolo on 2016/12/14.
 */
public class SearchException extends Exception{
    public SearchException(){
        super();
    }

    public SearchException(String s){
        super(s);
    }
}

class UnifitNodeException extends SearchException{
    public UnifitNodeException(){
        super();
    }

    public UnifitNodeException(String s){
        super(s);
    }
}

class UndefinedDateException extends SearchException{
    public UndefinedDateException(){
        super();
    }

    public UndefinedDateException(String s){
        super(s);
    }
}