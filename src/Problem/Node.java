package Problem; /**
 * Created by rolo on 2016/12/14.
 */

public class Node implements Cloneable, Comparable{
    private int[] data;
    public static final int UNDECIDED = -9999;
    public Node(int length){
        data = new int[length];
        for(int index = 0; index < length; index++){
            data[index] = UNDECIDED;
        }
    }

    public Node(int length, int[] data){
        if(length == data.length){
            this.data = data.clone();
        }
    }

    public int[] getData(){
        return data.clone();
    }

    public int[] accessData(){
        return data;
    }

    @Override
    public Object clone() throws CloneNotSupportedException{
        Node next = new Node(this.data.length);
        for(int index = 0; index < this.data.length;index ++)
            next.data[index] = this.data[index];
        return next;
    }

    @Override
    public int compareTo(Object o) {
        if(o.getClass() != this.getClass())
            return -2;
        if (this.data.length != ((Node)o).data.length)
            return -2;
        int counta = 0, countb = 0;
        for(int index = 0; index < this.data.length; index ++){
            if(this.data[index] == UNDECIDED)
                counta ++;
            if(((Node)o).data[index] == UNDECIDED)
                countb++;
        }
        if(counta > countb){
            return -1;
        }else if(counta < countb)
        {
            return 1;
        }
        else if(counta == countb){
            for(int index = 0; index < this.data.length; index++)
                if(this.data[index] != ((Node)o).data[index])
                    return 2;
        }
        return 0;

    }
}
