package Problem;

import java.util.ArrayList;

/**
 * Created by rolo on 2016/12/14.
 */

public class Problem {
    private int problemSize;

    public Problem(int problemSize){
        this.problemSize = problemSize;
    }

    public final int getProblemSize(){
        return problemSize;
    }

    /**
     * check if the current node fulfills the requirement that : 1. there is exactly n queens on the board; 2. none of
     * these n queens can be attacked by any other of them
     * @param current
     * @return true if current node is the goal, false if it's not
     * @throws SearchException
     */
    public boolean isGoal(Node current) throws SearchException {
        if(current.getData().length != problemSize)
            throw new UnifitNodeException("Problem.Problem size " + problemSize + " , node size" + current.getData().length);

        ArrayList<Integer> list = new ArrayList<Integer>();
        for(int a : current.getData()){
            if(a >= current.getData().length)
                throw new UndefinedDateException("Maximum coordinate is " + current.getData().length +
                " where as current queen coordinate is " + a);

            if(a == Node.UNDECIDED)
                return false;

            if(list.indexOf(a) == -1){
                list.add(a);
            }else return false;
        }

        for(int indexa = 0; indexa < current.getData().length; indexa ++){
            for(int indexb = indexa + 1; indexb < current.getData().length; indexb ++){
                if (checkBias(indexa, current.getData()[indexa],indexb, current.getData()[indexb]))
                    return false;
            }
        }
        return true;
    }

    /**
     * get a two-dimensional array to represent the available position on the board
     * @param current the node that represent current queens on the board
     * @return the two dimensional array with 1 represent the position is free from attack and not occupied
     * @throws SearchException
     */
    public int[][] getAvaliableList(Node current) throws SearchException {
        if(current.getData().length != this.problemSize)
            throw new UnifitNodeException("Problem.Problem size " + problemSize + " , node size" + current.getData().length);
        for(int a : current.getData()){
            if(a >= current.getData().length)
                throw new UndefinedDateException("Maximum coordinate is " + current.getData().length +
                        " where as current queen coordinate is " + a);
        }

        int[][] result = new int[problemSize][];

        for (int index = 0; index < problemSize; index++){
            result[index] = new int[problemSize];
            for (int indexa = 0; indexa < problemSize; indexa++){
                result[index][indexa] = 1;
            }
        }

        for(int index = 0; index < current.getData().length; index++){
            if (current.getData()[index] != Node.UNDECIDED){
                mark(index,current.getData()[index],result);
            }
        }

        return result;
    }

    private void mark(int q_x, int q_y, int[][] result){
        for (int indexa = 0; indexa < result.length; indexa ++){
            for (int indexb = 0; indexb < result.length; indexb++){
                result[q_x][indexb] = 0;
                result[indexa][q_y] = 0;
                if (checkBias(indexa, indexb, q_x, q_y))
                    result[indexa][indexb] = 0;
            }
        }
    }
    /**
     * check if two position on the board is on the same bias
     * @param firstX x coordinate of the first pos
     * @param firstY y ...
     * @param secondeX x ... second pos
     * @param secondY y ... second pos
     * @return true if queen x and queen y is on the same bias
     */
    public static boolean checkBias(int firstX, int firstY, int secondeX, int secondY){
       return Math.abs(firstX - secondeX) == Math.abs(firstY - secondY);
    }
}
