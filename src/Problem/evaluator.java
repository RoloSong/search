package Problem;

/**
 * Created by rolo on 2016/12/19.
 */
public abstract class evaluator {
    Problem problem;

    public evaluator(Problem problem){
        this.problem = problem;
    }

    /**
     * the evaluation function of the node current
     * @param current the node that represent current state
     * @return the grade of current state
     */
    public abstract int evaluation(Node current);
}
