package Problem;
import Agent.*;

/**
 * Default evaluator for state.
 * it assumes that the algorithm only choose available position when generate new node so it will not check the situation
 * that queens can attack each other.
 * for every queen that already be decided, get problemSize^2 scores+1
 * for every available position for next queens, get 1 score
 * such setting can make sure that the algorithm continues to put queens to the board
 * Created by rolo on 2016/12/19.
 */
public class DefaultEvaluator extends evaluator {
    public DefaultEvaluator(Problem p){
        super(p);
    }
    @Override
    public int evaluation(Node current) {
        //final int FAIL_FLAG = (int)Math.pow(problem.getProblemSize(),3.0);
        final int QUEEN_FLAG = current.getData().length * current.getData().length;
        final int AVIL_FLAG = 1;
        int result = 0;

        for (int a : current.getData())
            if(a != Node.UNDECIDED)
                result += QUEEN_FLAG;
        try {
            int [][] state = problem.getAvaliableList(current);
            for (int indexa = 0; indexa < state.length; indexa ++){
                for(int indexb = 0; indexb < state.length; indexb ++){
                    result += state[indexa][indexb] * AVIL_FLAG;
                }
            }
        } catch (SearchException e) {
            //e.printStackTrace();
        }

        return result;
    }
}
