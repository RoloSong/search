package Problem;

import Agent.Agent;
import javafx.event.EventHandler;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by rolo on 2017/1/2.
 */
public class Conflict {
    Problem problem;
    public Conflict(Problem problem){
        this.problem = problem;
    }

    public int getRandomConflictVaribleIndex(Node n){
        EvalResult List = getConflictVariableIndex(n);
        Random random = new Random();
        return List.result.get(random.nextInt(List.result.size()));
    }

    class EvalResult{
        ArrayList<Integer> result;
        int conflicValue;
        public EvalResult(){
            result = new ArrayList<>();
            conflicValue = 0;
        }
    }

    public EvalResult getConflictVariableIndex(Node n){
        EvalResult returnValue = new EvalResult();
        for(int index = 0; index < n.getData().length; index++){
            for(int indexb = index+1; indexb < n.getData().length; indexb++){
                if(n.getData()[index] == n.getData()[indexb]
                        ||
                        problem.checkBias(index,n.getData()[index],indexb,n.getData()[indexb]))
                {
                    if(!returnValue.result.contains(index))
                        returnValue.result.add(index);
                    if(!returnValue.result.contains(indexb))
                        returnValue.result.add(indexb);
                    returnValue.conflicValue += 1;
                }
            }
        }
        return returnValue;
    }

    public int getIdealPosition(Node n, int l){
        int pos = n.getData()[l];
        EvalResult current = getConflictVariableIndex(n);
        try{
            for(int index = 0; index < n.getData().length; index++){
                Node next = (Node)n.clone();
                next.accessData()[l] = index;
                EvalResult nextResult = getConflictVariableIndex(next);
                if(nextResult.conflicValue < current.conflicValue)
                {
                    current = nextResult;
                    pos = index;
                }
                else if(nextResult.conflicValue==current.conflicValue)
                {
                    if(Agent.probability(0.5)){
                        current = nextResult;
                        pos = index;
                    }
                }
            }
        }catch(Exception e){}
        return pos;
    }
}
