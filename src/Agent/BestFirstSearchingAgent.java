package Agent;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.PriorityQueue;

import Problem.*;

/**
 * Created by rolo on 2016/12/19.
 */
public class BestFirstSearchingAgent extends Agent {
    public void test(){
        int[] da = {1,Node.UNDECIDED,Node.UNDECIDED,Node.UNDECIDED};
        int[] da2 = {0,3,Node.UNDECIDED,Node.UNDECIDED};
        PriorityQueue<Node> pq = new PriorityQueue<>(new comp<Node>(new DefaultEvaluator(this.prob)));
        pq.add(new Node(4,da));
        pq.add(new Node(4,da2));
        System.out.println(pq.poll().getData()[0]);
        System.out.println(pq.poll().getData()[0]);
    }
    class comp<T extends Node> implements Comparator<T>{
        evaluator eva;
        public comp(evaluator eva){
            this.eva = eva;
        }
        @Override
        public int compare(T o1, T o2) {
            return eva.evaluation(o2) - eva.evaluation(o1);
        }
    }
    public BestFirstSearchingAgent(int problemSize){
        this.problemSize = problemSize;
        this.prob = new Problem(problemSize);
    }
    @Override
    public ArrayList<Node> search(boolean firstResult) {
        return search(new DefaultEvaluator(prob), firstResult);
    }

    public ArrayList<Node> search(evaluator eva, boolean firstResult){
        ArrayList<Node> visitedList = new ArrayList<>();
        ArrayList<Node> result = new ArrayList<>();
        PriorityQueue<Node> currentList = new PriorityQueue<Node>(new comp<Node>(eva));
        currentList.add(new Node(this.problemSize));

        while(!currentList.isEmpty()){
            Node current = currentList.poll();

            try{
                if(prob.isGoal(current)){
                    result.add(current);
                    if(firstResult)
                        return result;
                }
                else{

                    int[][] currentAvilList = prob.getAvaliableList(current);
                    for(int indexa = 0; indexa < currentAvilList.length;indexa++){
                        for(int indexb = 0; indexb < currentAvilList.length; indexb++){
                            if(currentAvilList[indexa][indexb] == 1)
                            {
                                Node next = (Node)current.clone();
                                next.accessData()[indexa] = indexb;

                                boolean addInFlag = true;
                                for (Node a :visitedList) {
                                    if(a.compareTo(next) == 0)
                                        addInFlag = false;
                                }
                                if(addInFlag){
                                    currentList.add(next);
                                    visitedList.add(next);

                                    searchedNode++;
                                }
                            }
                        }
                    }

                }
            }catch(SearchException e){
                System.out.println(e.getClass());
            }catch(CloneNotSupportedException e){}

        }
        return result;
    }
}
