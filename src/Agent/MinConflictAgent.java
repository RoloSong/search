package Agent;

import Problem.Conflict;
import Problem.Node;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by rolo on 2017/1/2.
 */
public class MinConflictAgent extends LocalSearchAgent {
    public MinConflictAgent(int problemSize) {
        super(problemSize);
        result_found = false;
        conflict = new Conflict(this.prob);
    }
    private Conflict conflict;
    public final int MAX_ITER_TIME = 5000;
    @Override
    public ArrayList<Node> search(boolean firstResult) {
        //Node current = new HillClimbingSearchingAgent(problemSize).search(true).get(0);
        int [] da = new int[problemSize];
        Random random = new Random();
        for(int index = 0; index < problemSize;index++)
            da[index] = random.nextInt(problemSize);
        Node current = new Node(problemSize, da);
        int count = 0;
        ArrayList<Node> result = new ArrayList<>();
        try {
            while (count < MAX_ITER_TIME) {

                System.out.println("iteration" +count);/*
                System.out.println("current");
                for(int a: current.getData())
                    System.out.print(a + " ");
                System.out.println();
*/
                if (this.prob.isGoal(current))
                {
                    result.add(current);
                    result_found = true;
                    return result;
                }
                //
                int line = conflict.getRandomConflictVaribleIndex(current);

                int pos = conflict.getIdealPosition(current,line);

                //System.out.println("line: " + line + " pos: " + pos);
                current.accessData()[line] = pos;
                //System.out.println("new current");
                /*
                for(int a : current.getData())
                    System.out.print(a + " ");
                System.out.println();
                System.out.println();*/
                count ++;
            }
        }catch(Exception e){}
        return result;
    }


}
