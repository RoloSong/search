package Agent;

import Problem.*;

import java.util.ArrayList;

/**
 * Created by rolo on 2016/12/20.
 */
public class BackTrackingSearchingAgent extends Agent{
    public BackTrackingSearchingAgent(int problemSize){
        super(problemSize);
    }
        DefaultConsumptionEvaluator DCE;
    @Override
    public ArrayList<Node> search(boolean firstResult) {
        ArrayList<Node> result = new ArrayList<>();
        DCE = new DefaultConsumptionEvaluator(prob);
        findLine(0,result, firstResult);
        try{
            for(int index = 0; index < result.size(); index++){
                if(!prob.isGoal(result.get(index))){
                    result.remove(index);
                }
            }
        }catch(Exception e){}
//        System.out.println("result size"+result.size());
        return result;
    }

    boolean findLine(int l, ArrayList<Node> result, boolean firstResult){
        if(l == 0){
            result.add(new Node(problemSize));
        }
        try{
            Node current = (Node)result.get(result.size()-1).clone();
            for(int index = 0; index < problemSize; index++){

                current.accessData()[l] = index;

                if(DCE.evaluation(current) == 0){

                    result.set(result.size()-1,(Node)current.clone());
                    if(l == this.problemSize-1)
                        return true;
                    if (findLine(l+1, result, firstResult)){
                        if(firstResult)
                            return true;
                        else result.add((Node)current.clone());
                    }
                }
            }
            return false;
        }catch(Exception e){}
        return false;
    }
}
