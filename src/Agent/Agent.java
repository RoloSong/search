package Agent; /**
 * Created by rolo on 2016/12/14.
 */
import java.util.ArrayList;
import java.util.Random;

import Problem.*;

public abstract class  Agent {
    Problem prob;
    int problemSize;
    public int searchedNode = 0;
    public Agent(int problemSize){
        this.problemSize = problemSize;
        this.prob = new Problem(problemSize);
    }
    public Agent(){

    }
    abstract public ArrayList<Node> search(boolean firstResult);
    public static boolean probability(double p){
        Random randomGenerator = new Random();
        if(randomGenerator.nextDouble() < p)
            return true;
        return false;
    }
}

