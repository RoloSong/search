package Agent;

import Problem.*;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by rolo on 2016/12/24.
 */
public class SimulatedAnnealingAgent extends LocalSearchAgent {
    public SimulatedAnnealingAgent(int problemSize){
        super(problemSize);
        this.Temperature = DEFAULT_TEMPERATURE;
        result_found = false;
    }
    public int Temperature;
    public final int DEFAULT_TEMPERATURE = Integer.MAX_VALUE;
    @Override
    public ArrayList<Node> search(boolean firstResult) {
        result_found = false;
        int currentT = Temperature;
        Random RandomGenerator = new Random();
        ArrayList<Node> result = new ArrayList<>();
        DefaultConsumptionEvaluator DCE = new DefaultConsumptionEvaluator(prob);
        int[] initialNodeData = new int[problemSize];
        for(int index = 0; index < initialNodeData.length; index++){
            initialNodeData[index] = RandomGenerator.nextInt(problemSize);
        }
        Node current = new Node(problemSize, initialNodeData);


        try{
            while(currentT > 0){
                //calculate acceptance
                //double acceptance = Math.pow(Math.E,currentT)/(Math.pow(Math.E,Temperature));
                //System.out.println("acceptance" + acceptance + " T " + currentT);
                //checkGoal
                if(prob.isGoal(current))
                {
                    result.add(current);
                    result_found = true;
                    return result;
                }
                else{
                    while(true){
                        Node next = (Node)current.clone();
                        next.accessData()[RandomGenerator.nextInt(problemSize)] = RandomGenerator.nextInt(problemSize);
                        if(DCE.evaluation(next) < DCE.evaluation(current)){
                            current = next;
                            break;
                        }
                        if(DCE.evaluation(next) >= DCE.evaluation(current)){
                            if(probability(Math.exp((DCE.evaluation(current)-DCE.evaluation(next))/currentT)))
                                current = next;
                                break;
                        }
                    }
                }
                currentT--;
            }
            result.add(current);

        }catch(Exception e){}
        return result;
    }
}
