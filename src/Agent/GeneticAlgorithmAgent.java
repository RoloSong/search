package Agent;

import Problem.*;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by rolo on 2016/12/24.
 */
public class GeneticAlgorithmAgent extends LocalSearchAgent {
    public GeneticAlgorithmAgent(int problemSize){
        super(problemSize);
        this.hordSize = DEFAULT_HORD_SIZE;
        this.mutationProbabilyty = DEFAULT_MUTATION_PROBABILITY;
        this.maximumGeneration = this.DEFAULT_MAXIMUM_GENERATION;
        result_found = false;
    }

    private int hordSize;
    public final int DEFAULT_HORD_SIZE = 40;
    private double mutationProbabilyty;
    public final double DEFAULT_MUTATION_PROBABILITY = 0.001;
    private int maximumGeneration;
    public final int DEFAULT_MAXIMUM_GENERATION = 100000;
    @Override
    public ArrayList<Node> search(boolean firstResult) {
        result_found = false;
        ArrayList<Node> result = new ArrayList<>();
        ArrayList<AugmentedNode> horde = new ArrayList<>();
        for(int index = 0; index < hordSize; index++)
            horde.add(new AugmentedNode(problemSize));
        ArrayList<AugmentedNode> nextHord = new ArrayList<>();
        ArrayList<AugmentedNode> selected = new ArrayList<>();
        try{
            for(int gene = 0; gene < maximumGeneration; gene++){
                //check if there is goal in the current horde
                for(int index = 0; index < horde.size(); index++)
                {
                    if(prob.isGoal(horde.get(index))){
                        result.add(horde.get(index));
                        result_found = true;
                        return result;
                    }
                }

                //---------------------------------------------
                /* the following section tries to generate next generation's horde */
                //first decide who will be selected
                selectMember(horde,selected);
                //then do hybridization
                for(int index = 0; index < selected.size(); index+=2){
                    hybridization(selected.get(index),selected.get(index+1),nextHord);
                }
                //then do random mutation
                for(int index = 0; index < nextHord.size(); index++){
                    nextHord.get(index).mutate(mutationProbabilyty);
                }
                //replace current horde with next horde
                horde = nextHord;
                nextHord = new ArrayList<>();
                //---------------------------------------------
            }
        }catch(SearchException e){
            System.out.println(e.toString());
        }
        AugmentedNode localBest = null;
        int localBestFitness = 0;
        for(AugmentedNode a : horde){
            if(a.getFitness() > localBestFitness){
                localBest = a;
                localBestFitness = a.fitness;
            }
        }
        result.add(localBest);
        //System.out.println("local best" + localBestFitness);
        return result;
    }

    private void selectMember(ArrayList<AugmentedNode> horde, ArrayList<AugmentedNode> selected){
        Random randomGenerator  = new Random();
        while(selected.size() < hordSize){
            int sum = 0;
            for(AugmentedNode a : horde)
                sum += a.getFitness();
            int selectedFlag = randomGenerator.nextInt(sum);
            int current = 0;
            int indexa;
            for(indexa = 0; indexa < horde.size(); indexa++){
                current += horde.get(indexa).getFitness();
                if(current >= selectedFlag)
                {
                    selected.add(horde.get(indexa));
                    break;
                }
            }

        }
    }

    private void hybridization(AugmentedNode parent1, AugmentedNode parent2, ArrayList<AugmentedNode> nextGeneration){
        AugmentedNode child1 = new AugmentedNode(parent1);
        AugmentedNode child2 = new AugmentedNode(parent2);
        Random randomGenerator = new Random();
        int flag = randomGenerator.nextInt(problemSize);
        for(int index = 0; index <= flag; index++){
            child1.accessData()[index] = parent2.getData()[index];
            child2.accessData()[index] = parent1.getData()[index];
        }
        nextGeneration.add(child1);
        nextGeneration.add(child2);
    }
    public static int mutateCount = 0;
    class AugmentedNode extends Node{

        public AugmentedNode(Node n){
            super(n.getData().length);
            for(int index = 0; index < n.getData().length; index++){
                this.accessData()[index] = n.getData()[index];
            }
            FE = new FitnessEvaluator(new Problem(n.getData().length));
            this.updateEvaluation();
        }

        public AugmentedNode(int probelmSize){
            super(probelmSize);
            Random randomGenerator = new Random();
            for(int index = 0; index < this.getData().length; index++){
                this.accessData()[index] = randomGenerator.nextInt(probelmSize);
            }
            FE = new FitnessEvaluator(new Problem(probelmSize));
            this.updateEvaluation();
        }


        public void mutate(double probability){
            Random randomGenerator = new Random();
            for(int index = 0; index < this.accessData().length; index++){
                if(Agent.probability(probability))
                {
                    mutateCount ++;
                    this.getData()[index] = randomGenerator.nextInt(this.getData().length);
                }
            }
            this.updateEvaluation();
        }
        private int fitness;
        private FitnessEvaluator FE;
        public void updateEvaluation(){
            this.fitness = FE.evaluation(this);
        }

        public int getFitness(){
            return fitness;
        }
    }
}
