package Agent;

import Problem.DefaultConsumptionEvaluator;
import Problem.Node;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by rolo on 2016/12/20.
 */
public class HillClimbingSearchingAgent extends LocalSearchAgent {
    public HillClimbingSearchingAgent(int problemSize){
        super(problemSize);
        result_found = false;
    }
    @Override
    public ArrayList<Node> search(boolean firstResult) {
        result_found = false;
        Random randomGenerator = new Random();
        ArrayList<Node> result = new ArrayList<>();
        //ArrayList<Node> visitedList = new ArrayList<>();
        DefaultConsumptionEvaluator DCE = new DefaultConsumptionEvaluator(prob);
        int[] initialNodeData = new int[problemSize];
        for(int index = 0; index < problemSize; index ++)
            initialNodeData[index] = randomGenerator.nextInt(problemSize);
        Node current = new Node(problemSize,initialNodeData);
        try{
            int minCom = DCE.evaluation(current);
            Node minNode = (Node)current.clone();
            int history = minCom;
            int history2 = history;
            Node next;
            int nextCom;
            int count = 0;
            while (count <= 10000){
                if(prob.isGoal(current)){
                    result.add(current);
                    result_found = true;
                    return result;
                }else{
                    history2 = history;
                    history = minCom;
                    for(int indexa = 0; indexa < current.getData().length; indexa++){
                        for(int indexb = 0; indexb < problemSize; indexb ++){
                            if(indexb != current.getData()[indexa]){
                                next = (Node)current.clone();
                                next.accessData()[indexa] = indexb;
                                nextCom = DCE.evaluation(next);
                                if(nextCom < minCom){

                                    minCom = nextCom;
                                    minNode = (Node)next.clone();
                                }
                            }
                        }
                    }
                    current = (Node)minNode.clone();

                    if(minCom == history2)
                    {
                        break;
                    }
                }
                count ++;
            }
            result.add(minNode);
        }catch(Exception e){}
        return result;
    }
}
