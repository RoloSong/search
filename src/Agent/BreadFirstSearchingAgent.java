package Agent;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import Problem.*;

/**
 * Agent.BreadFirstSearchingAgent class extends Agent.Agent class and implement abstract method search() that returns the node result
 * the search method was implemented with bread first search algorithm, which we decided
 */
public class BreadFirstSearchingAgent extends Agent{
    public BreadFirstSearchingAgent(int problemSize){
        this.problemSize = problemSize;
        this.prob = new Problem(this.problemSize);
    }

    @Override
    public ArrayList<Node> search(boolean firstResult){
        return this.search(new LinkedList<Node>(), firstResult);
    }

    public ArrayList<Node> search(Queue<Node> currentList, boolean firstResult){
        //LinkedList<Problem.Node> currentList = new LinkedList<>();

        ArrayList<Node> result = new ArrayList<>();
        int[] Nodedata = new int[problemSize];
        for(int index = 0; index < problemSize; index++)
            Nodedata[index] = Node.UNDECIDED;

        currentList.add(new Node(problemSize, Nodedata));
        while(!currentList.isEmpty()){
            Node current = currentList.poll();
            try{
                if(prob.isGoal(current)){
                    result.add(current);
                    if(firstResult)
                        return result;
                }
                else{
                    int[][] currentAvilList = prob.getAvaliableList(current);
                    for(int indexa = 0; indexa < currentAvilList.length; indexa++){
                        for(int indexb = 0; indexb < currentAvilList.length; indexb++){
                            if(currentAvilList[indexa][indexb] == 1){
                                Node next = (Node)current.clone();
                                next.accessData()[indexa] = indexb;
                                boolean addInFlag = true;


                                    for(int index = 0; index < currentList.size(); index++){
                                        if(((LinkedList<Node>)currentList).get(index).compareTo(next) == 0)
                                            addInFlag = false;
                                    }

                                if(addInFlag){
                                    currentList.add(next);
                                    searchedNode++;
                                }
                            }
                        }
                    }
                }
            }catch(Exception e){}

        }

        return result;
    }
}
