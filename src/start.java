/**
 * Created by rolo on 2016/12/15.
 */
import Agent.BreadFirstSearchingAgent;
import Problem.*;
import Agent.*;

import java.util.Date;

import java.util.ArrayList;
public class start {
    public static void main(String[] args){
        minC_test(100);
    }

    public static long test(Agent searchingAgent){
        Date start = new Date();
        searchingAgent.search(true);
        Date end = new Date();
        return end.getTime() - start.getTime();
    }


    public static void minC_test(int size){
        MinConflictAgent MCA = new MinConflictAgent(size);
        ArrayList<Node> result = MCA.search(true);
        for(Node p : result){
            int[][] currentState = NodeToState(p);
            printState(currentState);
            System.out.println();
        }
    }

    public static local_search_evaluate_data testOfLocalSearch(LocalSearchAgent searchingAgent){
        Date start = new Date();
        int index;
        for(index = 0; index < 1; index++){
            if(searchingAgent.result_found)
                break;
            searchingAgent.search(true);
        }
        Date end = new Date();
        local_search_evaluate_data result = new local_search_evaluate_data();
        result.time = end.getTime()-start.getTime();
        result.times = index;
        result.isSuccess = searchingAgent.result_found;
        return result;
    }

    public static void test(){
        int[] a = {1,3,0,2};
        Node current = new Node(4,a);
        FitnessEvaluator FE = new FitnessEvaluator(new Problem(4));
        System.out.println(FE.evaluation(current));
    }
    public static void testOfGoal(){
        Problem p = new Problem(4);
        int[] da = new int[4];
        da[0] = 1; da[1] = 3; da[2] = 0; da[3] = 2;
        Node n = new Node(4, da);
        try{
            System.out.println(p.isGoal(n));
            evaluator ev = new DefaultEvaluator(p);
            System.out.println(ev.evaluation(n));
        }catch (Exception e){}
    }

    public static void BreadFirstSearch(int problemSize){
        BreadFirstSearchingAgent BFSA = new BreadFirstSearchingAgent(problemSize);
        ArrayList<Node> result = BFSA.search(true);
        for(Node p : result){
            int[][] currentState = NodeToState(p);
            printState(currentState);
            System.out.println();
        }
        System.out.println("BFS" + BFSA.searchedNode);
    }

    public static void BestFirstSearch(int problemSize){
        BestFirstSearchingAgent BFSA = new BestFirstSearchingAgent(problemSize);
        ArrayList<Node> result = BFSA.search(true);
        for(Node p : result){
            int[][] currentState = NodeToState(p);
            printState(currentState);
            System.out.println();
        }
        System.out.println("BEFS" + BFSA.searchedNode);

    }

    public static void HillClimbingSearch(int problemSize){
        HillClimbingSearchingAgent HCSA = new HillClimbingSearchingAgent(problemSize);
        ArrayList<Node> result = HCSA.search(true);
        for(Node p : result){
            int [][] currentState = NodeToState(p);
            printState(currentState);
            System.out.println();
        }
    }

    public static void SimulatedAnnealingSearch(int problemSize){
        SimulatedAnnealingAgent SAA = new SimulatedAnnealingAgent(problemSize);
        ArrayList<Node> result = SAA.search(true);
        for(Node p : result){
            int[][] currentState = NodeToState(p);
            printState(currentState);
            System.out.println();
        }
    }

    public static void GeneticAlgorithmSearch(int problemSize){
        GeneticAlgorithmAgent GAA = new GeneticAlgorithmAgent(problemSize);
        ArrayList<Node> result = GAA.search(true);
        System.out.println("mutateC" + GAA.mutateCount);
        for(Node p : result){
            int[][] currentState = NodeToState(p);
            printState(currentState);
            System.out.println();
        }
    }

    public static void BackTracingSearch(int problemSize){
        BackTrackingSearchingAgent BTSA = new BackTrackingSearchingAgent(problemSize);
        ArrayList<Node> result = BTSA.search(false);
        Problem a = new Problem(problemSize);
        for(Node p : result){
            int [][] currentState = NodeToState(p);
            printState(currentState);
            System.out.println();
        }
    }

    public static int[][] NodeToState(Node current){
        int[][] result = new int[current.getData().length][current.getData().length];
        for(int indexa = 0; indexa < current.getData().length; indexa ++){
            for(int indexb = 0; indexb < current.getData().length; indexb++){
                result[indexa][indexb] = 0;
            }
            result[indexa][current.getData()[indexa]] = 1;
        }
        return result;
    }

    public static void printState(int[][] currentState){
        for(int indexa = 0; indexa < currentState.length; indexa ++){
            for(int indexb = 0; indexb < currentState.length; indexb++){
                System.out.print(" " + currentState[indexa][indexb] + " ");
            }
            System.out.println();
        }

    }
}

class local_search_evaluate_data{
    long time;
    int times;
    boolean isSuccess;
}